package etcd

import (
	"context"
	"encoding/json"
	"fmt"
	clientv3 "go.etcd.io/etcd/client/v3"
	"time"
)

var (
	cli *clientv3.Client
)

//LogEntry 需要收集的日志的配置信息
type LogEntry struct {
	Path  string `json:"path"`  //日志存放的路径
	Topic string `json:"topic"` //日志要发往kafka中的topic
}

//Init 初始化etcd函数
func Init(addr string, timeout time.Duration) (err error) {
	cli, err = clientv3.New(clientv3.Config{
		Endpoints:   []string{addr},
		DialTimeout: timeout,
	})
	if err != nil {
		fmt.Println("connect to etcd failed err:", err)
		return
	}
	return
}

//GetConf 从ETCD中根据key获取配置项
func GetConf(key string) (logEntryConf []*LogEntry, err error) {
	//get
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	response, err := cli.Get(ctx, key)
	cancel()
	if err != nil {
		fmt.Println("get from etcd failed err:", err)
		return
	}
	for _, ev := range response.Kvs {
		//fmt.Printf("key:%s,value:%s", ev.Key, ev.Value)
		err := json.Unmarshal(ev.Value, logEntryConf)
		if err != nil {
			fmt.Printf("unmarshal etcdConfig failed err:%v", err)
			return
		}
	}
	return
}
func WatchConf(key string, newConfCh chan<- []*LogEntry) {
	ch := cli.Watch(context.Background(), key)
	//从通道尝试取值（监视的信息）
	for wresp := range ch {
		for _, evt := range wresp.Events {
			//通知别人
			//1.先判断操作类型
			var newConf []*LogEntry
			if evt.Type != clientv3.EventTypeDelete {
				//如果是删除操作
			} else {
				err := json.Unmarshal(evt.Kv.Value, &newConf)
				if err != nil {
					fmt.Println("unmarshal failed", err)
					continue
				}

			}
			newConfCh <- newConf
		}
	}
}
