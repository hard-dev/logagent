package main

import (
	"fmt"
	"gitee.com/hard-dev/logagent/conf"
	"gitee.com/hard-dev/logagent/etcd"
	"gitee.com/hard-dev/logagent/kafka"
	"gitee.com/hard-dev/logagent/taillog"
	"gitee.com/hard-dev/logagent/utils"
	"gopkg.in/ini.v1"
	"sync"
	"time"
)

var cfg = new(conf.AppConf)

//func run() {
//	//1.读取日志
//	for {
//		select {
//		case line := <-tail_log.ReadChan():
//			//2.发送到kafka
//			kafka.SendToKafka(cfg.KafkaConf.Topic, line.Text)
//		default:
//			time.Sleep(time.Second)
//		}
//
//	}
//
//}

//logAgent入口程序
func main() {
	//加载配置文件
	err := ini.MapTo(cfg, "./conf/config.ini")
	if err != nil {
		fmt.Println("load config failed,err:", err)
		return
	}
	//1.初始化kafka连接
	err = kafka.Init([]string{cfg.KafkaConf.Address}, cfg.KafkaConf.ChanMaxSize)
	if err != nil {
		fmt.Println("init kafka failed err:", err)
		return
	}
	fmt.Println("init success")
	//初始化etcd
	err = etcd.Init(cfg.EtcdConf.Address, time.Duration(cfg.EtcdConf.Timeout)*time.Second)
	if err != nil {
		fmt.Println("client etcd failed err:", err)
		return
	}
	//为了实现每一个logagent都实现拉去自己的配置 所以要以自己的ip地址作为区分
	ipStr, err := utils.GetOutBoundIp()
	if err != nil {
		panic(err)
	}
	EtcdConfKey := fmt.Sprintf(cfg.EtcdConf.Key, ipStr)
	//2.1从etcd中获取日志收集项配置信息
	logEntryConf, err := etcd.GetConf(EtcdConfKey)
	if err != nil {
		fmt.Println("get config failed err:", err)
	}
	fmt.Printf("get conf from etcd success,logEntryConf:%v", logEntryConf)

	//3.1收集日志发往kafka
	//3.2循环每一个日志项，创建TailObj
	taillog.Init(logEntryConf)
	//2.2派一个哨兵去监视日志收集项的变化（有变化及时通知我的LogAgent 实现热加载配置）
	newConfChan := taillog.NewConfChan() //从taillog包中获取对外暴漏通道
	var wg sync.WaitGroup
	wg.Add(1)
	go etcd.WatchConf(EtcdConfKey, newConfChan) //哨兵发现最新的信息会通知上面的通道
	wg.Wait()
	//打开日志文件准备收集日志
	//err = tail_log.Init(cfg.TailLog.FileName)
	//if err != nil {
	//	fmt.Println("tail_log init failed err:", err)
	//	return
	//}
	//fmt.Println("init taillog success")

	//run()
}
