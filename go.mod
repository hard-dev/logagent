module gitee.com/hard-dev/logagent

go 1.16

require github.com/Shopify/sarama v1.29.1 // indirect

require (
	github.com/coreos/etcd v2.3.8+incompatible // indirect
	github.com/fsnotify/fsnotify v1.5.1 // indirect
	github.com/hpcloud/tail v1.0.0
	go.etcd.io/etcd v2.3.8+incompatible // indirect
	go.etcd.io/etcd/client/v3 v3.5.0 // indirect
	go.uber.org/zap v1.19.0 // indirect
	gopkg.in/ini.v1 v1.62.0 // indirect
	gopkg.in/tomb.v1 v1.0.0-20141024135613-dd632973f1e7 // indirect
)
