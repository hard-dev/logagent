package taillog

import (
	"context"
	"fmt"
	"gitee.com/hard-dev/logagent/kafka"
	"github.com/hpcloud/tail"
)

/*var (
	tailObj *tail.Tail
	LogChan chan string
)*/

type TailTask struct {
	path     string
	topic    string
	instance *tail.Tail
	//为了能够实现退出t.run()
	ctx        context.Context
	cancelFunc context.CancelFunc
}

func NewTailTask(path, topic string) (tailObj *TailTask) {
	ctx, cancel := context.WithCancel(context.Background())
	tailObj = &TailTask{
		path:       path,
		topic:      topic,
		instance:   nil,
		ctx:        ctx,
		cancelFunc: cancel,
	}
	tailObj.init()
	return
}
func (t TailTask) init() {
	config := tail.Config{
		ReOpen: true, //重新打开
		Follow: true, //是否跟随
		Location: &tail.SeekInfo{ //文件从哪个地方开始读
			Offset: 0,
			Whence: 2,
		},
		MustExist: false, //文件不存在不报错
		Poll:      true,  //轮询更改而不通知
	}
	var err error
	t.instance, err = tail.TailFile(t.path, config)
	if err != nil {
		fmt.Println("init tail_log failed err:", err)
		return
	}
	//当goroutine执行的函数退出的时候 go推出了
	go t.run() //直接去采集日志发送到kafka
}

//专门收集日志的模块

/*func Init(filename string) (err error) {
	config := tail.Config{
		ReOpen: true, //重新打开
		Follow: true, //是否跟随
		Location: &tail.SeekInfo{ //文件从哪个地方开始读
			Offset: 0,
			Whence: 2,
		},
		MustExist: false, //文件不存在不报错
		Poll:      true,  //轮询更改而不通知
	}
	tailObj, err = tail.TailFile(filename, config)
	if err != nil {
		fmt1.Println("get filename or config failed err:",
			err)
		return
	}
	return
}*/
/*func (t *TailTask) ReadChan() <-chan *tail.Line {
return t.instance.Lines
/*var (
	line *tail.Line
	ok   bool
)
for {
	line, ok = <-tailObj.Lines
	if !ok {
		fmt1.Printf("tail file close reopen ,filename:%s\n", tailObj.Filename)
		time.Sleep(time.Second)
		continue
	}*/
//}
//*/
func (t *TailTask) run() {
	for {
		select {
		case <-t.ctx.Done():
			fmt.Printf("tail task :%s_%s 退出了", t.path, t.topic)
			return
		case line := <-t.instance.Lines: //从tailObj的通道中一行一行的读取数据
			//发往kafka
			//kafka.SendToKafka(t.topic, line.Text)
			//先把数据发送到一个通道中
			kafka.SendToChan(t.topic, line.Text)
		}
	}
}
