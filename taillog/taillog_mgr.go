package taillog

import (
	"fmt"
	"gitee.com/hard-dev/logagent/etcd"
	"time"
)

var tskMgr *tailLogMgr

type tailLogMgr struct {
	LogEntry    []*etcd.LogEntry
	TskMap      map[string]*TailTask
	newConfChan chan []*etcd.LogEntry
}

func Init(logEntryConf []*etcd.LogEntry) {
	tskMgr = &tailLogMgr{
		LogEntry:    logEntryConf,
		TskMap:      make(map[string]*TailTask, 16),
		newConfChan: make(chan []*etcd.LogEntry),
	}
	for _, logEntry := range logEntryConf {
		//logEntry*etcd.LogEntry
		//初始化时起了多少tailTask个都要记下来，为了后续判断方便
		mk := fmt.Sprintf("%s_%s", logEntry.Path, logEntry.Topic)
		tailObj := NewTailTask(logEntry.Path, logEntry.Topic)
		tskMgr.TskMap[mk] = tailObj
		return
	}
}

//ran 监听自己的newConfChan通道  有了新配置过来之后做处理

func (t *tailLogMgr) ran() {
	for {
		select {
		case newConf := <-t.newConfChan:
			//1.配置新增
			for _, conf := range newConf {
				mk := fmt.Sprintf("%s_%s", conf.Path, conf.Topic)
				_, ok := t.TskMap[mk]
				if ok {
					//原来就有，不需要操作
				} else {
					//新增的
					tailObj := NewTailTask(conf.Path, conf.Topic)
					t.TskMap[mk] = tailObj
				}
			}
			//找出t.logEntry中有 但newConf中没有的 要删掉
			for _, c1 := range t.LogEntry {
				isDelete := true
				for _, c2 := range newConf {
					if c2.Path == c1.Path && c2.Topic == c1.Topic {
						isDelete = false
						continue
					}
				}
				if isDelete {
					//把c1中对应的tailObj给停掉
					mk := fmt.Sprintf("%s_%s", c1.Path, c1.Topic)
					t.TskMap[mk].cancelFunc()
				}
			}
			//2.配置删除
			//3.配置变更
			fmt.Println("新的conf来了", newConf)
		default:
			time.Sleep(time.Second)
		}
	}
}

//向外暴漏一个函数，项tskMgr的newConfChan

func NewConfChan() chan<- []*etcd.LogEntry {
	return tskMgr.newConfChan
}
