package utils

import (
	"fmt"
	"net"
	"strings"
)

//GetOutBoundIp 获取本地对外ip
func GetOutBoundIp() (ip string, err error) {
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		fmt.Println("get bound ip failed err:", err)
		return
	}
	defer conn.Close()
	localAdder := conn.LocalAddr().(*net.UDPAddr)
	ip = strings.Split(localAdder.IP.String(), ":")[0]
	return
}
